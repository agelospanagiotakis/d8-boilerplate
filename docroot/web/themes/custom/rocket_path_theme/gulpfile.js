'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var exec = require('child-process-promise').exec;
var logger = require("eazy-logger").Logger();
var svgSprite = require('gulp-svg-sprite');
var config = {
    mode        : {
        symbol      : true    // Activate the «symbol» mode
    }
};

browserSync.init({
  socket: {
    domain: 'localhost:3000'
  }
});

gulp.task('rocket_path_theme:sass', function () {
  gulp.src('./style/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: [
        './node_modules/breakpoint-sass/stylesheets/',
        './node_modules/compass-mixins/lib/',
        './node_modules/bootstrap-sass/assets/stylesheets/',
        './node_modules/font-awesome/scss/'
      ]
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 version']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./style/css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
  });

gulp.task('rocket_path_theme:watch', function () {
  gulp.watch('./style/scss/**/*.scss', ['rocket_path_theme:sass']);
});

gulp.task('default', ['rocket_path_theme:sass', 'rocket_path_theme:watch']);


var async = require('async');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');

gulp.task('Iconfont', function(done){
  var iconStream = gulp.src(['./svg/*.svg'])
    .pipe(iconfont({ fontName: 'myfont', normalize:true,
            fontHeight: 1001 }));

  async.parallel([
    function handleGlyphs (cb) {
      iconStream.on('glyphs', function(glyphs, options) {
        console.log(glyphs);
        gulp.src('./fonticon/_icons.scss')
          .pipe(consolidate('lodash', {
            glyphs: glyphs,
            fontName: 'myfont',
            appendUnicode: true,
            fontPath: '../../fonticon/fonts/',
            className: 'font-icon',
            iconClass: 'icon'
          }))
          .pipe(gulp.dest('./style/scss/variables/'))
          .on('finish', cb);
      });
    },
    function handleFonts (cb) {
      iconStream
        .pipe(gulp.dest('./fonticon/fonts/'))
        .on('finish', cb);
    }
  ], done);
});

gulp.task('default', ['rocket_path_theme:sass', 'rocket_path_theme:watch', 'Iconfont']);
