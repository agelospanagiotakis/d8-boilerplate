(function ($) {

  Drupal.behaviors.rocket_path_themeNavigation = {
    attach: function (context, settings) {
      var $context = $(context);

      // $('#mobile-menu', context).bind('click', function(e) {
      //   e.preventDefault();
      //   $("html, body").animate({ scrollTop: 0 }, 0);
      //   $('body').toggleClass('mobile-menu-active');
      // });

      $('.burger-wrapper .search-button', context).bind('click', function(e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, 0);
        $('body').toggleClass('mobile-menu-active');
      });

      // $('.l-mobile-overlay, .mobile-menu-close', context).bind('click', function(e) {
      //   e.preventDefault();
      //   $('body').removeClass('mobile-menu-active');
      // });
    }
  }

  $('.main-menu').clone().appendTo('.l-mobile-menu');

  // Default behavior.
  Drupal.behaviors.rocket_path_theme = {
    attach: function (context, settings) {
      var $context = $(context);

      $(document).scroll(function() {
        if ($('body').width() < 720) {
          if ($(document).scrollTop() > 59) {
            $('body').addClass('scroll-menu');
          }
          else {
            $('body').removeClass('scroll-menu');
          }
        }
      });

      $('.paragraph--type--services .field--name-field-service-title', context).bind('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).parent('.paragraph--type--services').toggleClass('opened');
      });


      $(window, context).once('rocket_path_theme').on('load',function(){
        $('#rocketSlider').lightSlider({
          controls: true,
          gallery: true,
          pager: true,
          item: 1,
          slideMargin: 0,
          thumbItem: 5
        });
      });

      $('#block-headeractions .weather').bind('click', function(e) {
          $('#block-weatherblock').addClass('show');
      });

      $('#block-weatherblock .close-weather').bind('click', function(e) {
          $('#block-weatherblock').removeClass('show');
      });

      $('.details-tabs li a').click(function(e) {
        e.preventDefault();
        $('.tabs-wrapper .active-tab').removeClass('active-tab');
        $('.details-tabs li a').removeClass('active');
        $('.tabs-wrapper #' + $(this).attr('class')).addClass('active-tab');
        $(this).addClass('active');
      });

      $('.bottom-section.till-mobile .product-tab').click(function(e) {
        e.preventDefault();

        $('.bottom-section.till-mobile .tab-wrapper').removeClass('open-tab-mob');

        if($(this).closest('.tab-wrapper').hasClass('active-tab-mob')) {
          $(this).closest('.tab-wrapper').removeClass('open-tab-mob');
          $(this).closest('.tab-wrapper').removeClass('active-tab-mob');
        }
        else {
          $(this).closest('.tab-wrapper').addClass('open-tab-mob');
          $('.bottom-section.till-mobile .tab-wrapper').removeClass('active-tab-mob');
          $(this).closest('.tab-wrapper').addClass('active-tab-mob');
        }
      });

      $('.term-info-banner').css('background-color', '#' + $('.term-info-banner').attr('data-color'));

      // Messages.
      $('div.messages', context).find('button.close').bind('click', function(e) {
        e.preventDefault();
        $(this).closest('div.messages').slideUp(300, function() {
          $(this).remove();
        });
      });

      // Add filled class to input elements.
      $('input.form-text', context).bind('focus', function() {
        $(this).closest('.form-item').addClass('form-animate')
      }).bind('keyup', function() {
        if (this.value !== '') {
          $(this).closest('.form-item').addClass('filled');
        }
      }).bind('change blur', function() {
        if (this.value === '') {
          $(this).closest('.form-item').removeClass('filled');
        }
        else {
          $(this).closest('.form-item').addClass('filled');
        }
      }).trigger('blur');
    }
  }

  // Show dropdown.
  function showdropdown() {
    $(this).addClass('hover');
    $(this).children('a').addClass('hover');
    clearInterval(this.timer);
  }

  // Hide dropdown.
  function hidedropdown() {
    $(this).removeClass('hover');
    $(this).children('a').removeClass('hover');
    clearInterval(this.timer);
  }

  Function.prototype.mybind = function(object) {
    var method = this;
    return function() {
      method.apply(object, arguments);
    };
  };


})(jQuery);



//Mobile slider menu

( function( $ ) {

  if ( $(window).width() < 980) {

    //console.log('Evoke slider menu for mobile');
    $( '#block-rocket-path-theme-main-menu > ul.menu' ).sliderMenu();
    //console.log('Done');

    $(".burger-wrapper #mobile-menu").on("click", function(){
      console.log('Clicked');
      $("body").toggleClass("menu-open");
    });
  }

})( jQuery );
