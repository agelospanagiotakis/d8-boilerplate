(function ($) {
    var dgmap_canvasName = '.view-markers-map .geolocation-common-map-container';
    var dgmap = null;
    var mapDirDiv;
    var myPosMarker;
    var markers;
    var dro = {suppressMarkers: true};
    var directionsDisplay;
    var directionsService;
    var geocoder;
    var markerArray = [];
    var debugMap =true;
    var Cbounds ;
  
    var styles = [{
      stylers: [{
        'invert_lightness': false
      }, {
        saturation: 0
      }]
    }, {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{
        lightness: 0
      }, {
        visibility: 'on'
      }]
    }, {}];
  
    var styledMap;
  
    $(document).ready(function () {
      setTimeout(checkMapVariable, 1000);

      $(".search-map-form-submit").bind('click', function (){
        valueAddress =  $(".search-map-form-location").val();

        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': valueAddress}, function (results, status) {
          if (status === 'OK') {
            if (results[0]) {
              var position = results[0].geometry.location;
              var str_address = results[0].formatted_address;

              $('.search-map-form-location').val(str_address);
              if (position) {
                createAndAddToMap(position);
                var dest = findClosestPoint(position);
                if (dest){
                  showDirections(position, dest);
                }
              }
            }
            else {
              console.log('No results found');
            }
          }
          else {
            console.log('Geocoder failed due to: ' + status);
          }
        });
      });
    });
  
    function checkMapVariable () {
      if (Drupal.geolocation !== undefined) {
        if (Drupal.geolocation.maps  !== null) {
          if (Drupal.geolocation.maps[0].googleMap !== null) {
            directionsDisplay = new google.maps.DirectionsRenderer(dro);
            directionsService = new google.maps.DirectionsService();
  
            waitedForMap();
          }
        }
      }
    }
  
    function waitedForMap () {
      dgmap = Drupal.geolocation.maps[0].googleMap;
      initMe();
    }
  
    function initMe () {
      dgmap = Drupal.geolocation.maps[0].googleMap;
      styledMap = new google.maps.StyledMapType(
        [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},
        {"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},
        {"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
        {"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},
        {"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
        {"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},
        {"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},
        {"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},
        {"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},
        {"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}],
        {name:"styled_map"}
      );
      dgmap.mapTypes.set('styled_map', styledMap);
      dgmap.setMapTypeId('styled_map');

       //Drupal.geolocation.getMap(dgmap_canvasName).map;
      fixMarkersIcon();
      var param1_origin = getUrlParameter('field_geofield_distance[origin]');
      param1_origin = param1_origin == undefined ? '' : param1_origin.trim();

      var position = null;
      var FindByCurrLocation = false;
      if ((param1_origin === undefined) || (param1_origin == '')) {
        FindByCurrLocation = true;
      }
      // if (FindByCurrLocation) {
      //   getInitGeoLocation();
      // }
      // else {
        if (param1_origin) {
          geocoder = new google.maps.Geocoder();
          geocoder.geocode({'address': param1_origin}, function (results, status) {
            if (status === 'OK') {
              if (results[0]) {
  
                var position = results[0].geometry.location;
                var str_address = results[0].formatted_address;
                $('#edit-field-geofield-distance-origin').val(str_address);
                if (position) {
                  createAndAddToMap(position);
                  var dest = findClosestPoint(position);
                  if (dest){
                    showDirections(position, dest);
                  }
                }
              }
              else {
                console.log('No results found');
              }
            }
            else {
              console.log('Geocoder failed due to: ' + status);
            }
          });
  
        }
      //}
    }
  
    function PostCallback(position) {  
      if (position) {
        createAndAddToMap(position);
        convertPos2Address(position.position);
        var dest = findClosestPoint(myPosMarker);
        if (dest){
          showDirections(position, dest);
        }
      }
    }
  
    function convertPos2Address(pos) {
      geocoder = new google.maps.Geocoder()
      geocoder.geocode({'location': pos}, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {
            var str_address = results[0].formatted_address;
            $('#edit-field-geofield-distance-origin').val(str_address);
          }
        }
        else {
          console.log('Geocoder failed due to: ', results, status)
        }
      })
    }
  
    function findClosestPoint(currLocation) {
      dgmap = Drupal.geolocation.maps[0].googleMap;
      markers = Drupal.geolocation.maps[0].mapMarkers; //dgmap.markers;
      var distances = [];
      var foundMarkers2Items = [];
      var foundMarkers2Item_NIDS = [];
      var foundMarkers2Item_paths = [];
      var closest = -1;
      if (markers) {
        if (markers !== null) {
              for (var i = 0; i < markers.length; i++) {
                  var thisMarker = markers[i];
                  if (thisMarker.position.lat() == NaN) {continue;}
                  if (thisMarker.position.lng() == NaN) {continue;}
                  if (currLocation.position){
                      currLocation= currLocation.position;
                  }
                  var d = google.maps.geometry.spherical.computeDistanceBetween(currLocation, thisMarker.position);
                  distances[i] = d;
                  if (closest == -1 || d < distances[closest]) {
                      closest = i;
                  }
              }

          }
      }

      if (closest != -1) {
          return markers[closest].position;
      }
      return null;
    }
  
    function createAndAddToMap(pos) {
      var imageOfStart = "";
      imageOfStart = '/themes/custom/rocket_path_theme/images/curr_pin.svg';
  
      myPosMarker = new google.maps.Marker({
        map: dgmap,
        animation: google.maps.Animation.DROP,
        position: pos,
        icon: imageOfStart
      });
      myPosMarker.addListener('click', function () {
        infowindow.open(dgmap, myPosMarker)
      });
      var contentString = '<div id="content">' +
        'your Current Location' +
        '</div>';
  
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      })
    }
  
    function fixMarkersIcon() {
      dgmap = Drupal.geolocation.maps[0].googleMap;
      markers = dgmap.markers;
      if (markers){
        markers.forEach(function (marker) {
            marker.setIcon({
                url: '/themes/custom/rocket_path_theme/images/pin.svg',
                scaledSize: new google.maps.Size(40, 40)
            });
        });
      }
    }
  
    function convertBothAddresses(pos1, pos2, handleData) {
      var geocoder1 = new google.maps.Geocoder()
      geocoder1.geocode({'location': pos1}, function (results1, status1) {
        if (status1 === 'OK') {
          if (results1[0]) {
            var str_address1 = results1[0].formatted_address;
            geocoder2 = new google.maps.Geocoder()
            geocoder2.geocode({'location': pos2}, function (results2, status2) {
              if (status2 === 'OK') {
                if (results2[0]) {
                  var str_address2 = results2[0].formatted_address;
                  handleData(str_address1,str_address2);
                }
                else {
                  console.log('No results found')
                }
              }
              else {
                console.log('2)Geocoder failed due to: ', results2, status2)
              }
            })
  
  
          }
          else {
            console.log('No results found')
          }
        }
        else {
          console.log('1)Geocoder failed due to: ', results1, status1)
        }
      })
    }
  
    // https://developers.google.com/maps/documentation/javascript/examples/directions-complex
    function showDirections(pos, dest) {
      if (!dest) return;
      if (!pos) return;
      var l1_lat = pos.lat;
      var l1_lng = pos.lng;
      var l2_lat = dest.lat;
      var l2_lng = dest.lng;
      var startP = new google.maps.LatLng(l1_lat, l1_lng);
      var endP = new google.maps.LatLng(l2_lat, l2_lng);
  
      //convertBothAddresses(startP,endP,function(output1,output2){
      convertBothAddresses(pos,dest,function(output1,output2){
        // here you use the output
        directionsDisplay.setPanel(document.getElementById('directionsPanel'));
        dgmap.mapTypes.set('map_style', styledMap);
        dgmap.setMapTypeId('map_style');
        var request = {
          origin: output1,
          destination: output2,
          travelMode: google.maps.TravelMode['DRIVING']
          //, optimizeWaypoints: true
        }
        directionsDisplay.setMap(dgmap);
        directionsService.route(request, function (response, status) {
          // Route the directions and pass the response to a function to create
          // markers for each step.
          if (status == google.maps.DirectionsStatus.OK) {
            //directionsDisplay.setDirections(response);
            directionsDisplay.setDirections(response)
          }
          else if (status == 'ZERO_RESULTS') {
            console.log('No route could be found between the origin and destination.')
          }
          else if (status == 'UNKNOWN_ERROR') {
            console.log('A directions request could not be processed due to a server error. The request may succeed if you try again.')
          }
          else if (status == 'REQUEST_DENIED') {
            console.log('This webpage is not allowed to use the directions service.')
          }
          else if (status == 'OVER_QUERY_LIMIT') {
            console.log('The webpage has gone over the requests limit in too short a period of time.')
          }
          else if (status == 'NOT_FOUND') {
            console.log('There is no place what you have searched.')
          }
          else if (status == 'INVALID_REQUEST') {
            console.log('The DirectionsRequest provided was invalid.')
          }
          else {
            console.log('There was an unknown error in your request. Requeststatus: \n\n' + status)
          }
        });
  
  
        // Instantiate an info window to hold step text.
        var stepDisplay = new google.maps.InfoWindow
        // Display the route between the initial start and end selections.
        //calculateAndDisplayRoute(startP, endP);
        var currZoom = dgmap.getZoom();
        Cbounds = new google.maps.LatLngBounds(pos, dest);
        var zb = getZoomByBounds(dgmap, Cbounds);
        var wanted_zoom_level_after_bounds = 11;
  
        google.maps.event.addListener(dgmap, 'zoom_changed', function() {
          zoomChangeBoundsListener =
              google.maps.event.addListener(dgmap, 'bounds_changed', function(event) {
                zl = this.getZoom()
                  if (zl > wanted_zoom_level_after_bounds && this.initialZoom == true) {
                      // Change max/min zoom here
                      this.setZoom(wanted_zoom_level_after_bounds);
                  }
                  if (zl == wanted_zoom_level_after_bounds){
                    dgmap.initialZoom = false;
                    google.maps.event.removeListener(zoomChangeBoundsListener);
                  }
          });
      });
      dgmap.initialZoom = true;
      dgmap.setCenter(Cbounds.getCenter(),zb);
      dgmap.fitBounds(Cbounds);
    });
  }
  
  
  function ZoomToPoint(lat,lng) {
    var markers = dgmap.markers;
    var location = null;
    for( i = 0; i < markers.length; i++ ) {
      var currPosition =  markers[i].geometry.location;
      if ( currPosition) {
        if ( currPosition.coords.longitude == lng ) {
          if ( currPosition.coords.latitude == lat ) {
            location = currPosition;
            break;
          }
        }
      }
    }
    if (location){
      map.setCenter(location);
      map.setZoom(10);
    }
  }
  
    function calculateAndDisplayRoute(startP, endP) {
      var request = {
        origin: startP,
        destination: endP,
        travelMode: google.maps.TravelMode['DRIVING'],
        optimizeWaypoints: true
      }
      directionsDisplay.setMap(dgmap);
  
      directionsService.route(request, function (response, status) {
        // Route the directions and pass the response to a function to create
        // markers for each step.
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response)
        }
        else if (status == 'ZERO_RESULTS') {
          console.log('No route could be found between the origin and destination.')
        }
        else if (status == 'UNKNOWN_ERROR') {
          console.log('A directions request could not be processed due to a server error. The request may succeed if you try again.')
        }
        else if (status == 'REQUEST_DENIED') {
          console.log('This webpage is not allowed to use the directions service.')
        }
        else if (status == 'OVER_QUERY_LIMIT') {
          console.log('The webpage has gone over the requests limit in too short a period of time.')
        }
        else if (status == 'NOT_FOUND') {
          console.log('There is no place what you have searched.')
        }
        else if (status == 'INVALID_REQUEST') {
          console.log('The DirectionsRequest provided was invalid.')
        }
        else {
          console.log('There was an unknown error in your request. Requeststatus: \n\n' + status)
        }
      });
    }
  
    function showSteps(directionResult, markerArray, stepDisplay, map) {
      // For each step, place a marker, and add the text to the marker's
      // infowindow. Also attach the marker to an array so we can keep track of
      // it and remove it when calculating new routes.
      var myRoute = directionResult.routes[0].legs[0]
      for (var i = 0; i < myRoute.steps.length; i++) {
        attachInstructionText(
          stepDisplay, marker, myRoute.steps[i].instructions, map)
      }
    }
  
    function attachInstructionText(stepDisplay, marker, text, map) {
      google.maps.event.addListener(marker, 'click', function () {
        // Open an info window when the marker is clicked on, containing the text
        // of the step.
        stepDisplay.setContent(text)
        stepDisplay.open(map, marker)
      })
    }
  
    // Try HTML5 geolocation.
    function getInitGeoLocation () {
      if (navigator.geolocation) {
        // todo: check if only not a location is entered!
        navigator.geolocation.getCurrentPosition(function (position) {
            var position1 = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            }
            PostCallback(position1);
          }, function (err) {
            $.getJSON('https://ipinfo.io', function (ipinfo) {
              var latLong = ipinfo.loc.split(',');
              var position2 = {
                lat: parseFloat(latLong[0]),
                lng: parseFloat(latLong[1])
              }
              PostCallback(position2);
            })
          },
          {timeout: 30000}
        )
      }
    }
  
    function getUrlParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1))
      var sURLVariables = sPageURL.split('&')
      var sParameterName
      var i
  
      for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=')
  
        if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : sParameterName[1]
        }
      }
    }
  
    /**
     * Returns the zoom level at which the given rectangular region fits in the map view.
     * The zoom level is computed for the currently selected map type.
     * @param {google.maps.Map} map
     * @param {google.maps.LatLngBounds} bounds
     * @return {Number} zoom level
     **/
    function getZoomByBounds(map, bounds) {
      var MAX_ZOOM = map.mapTypes.get(map.getMapTypeId()).maxZoom || 21;
      var MIN_ZOOM = map.mapTypes.get(map.getMapTypeId()).minZoom || 0;
  
      var ne = map.getProjection().fromLatLngToPoint(bounds.getNorthEast());
      var sw = map.getProjection().fromLatLngToPoint(bounds.getSouthWest());
  
      var worldCoordWidth = Math.abs(ne.x - sw.x);
      var worldCoordHeight = Math.abs(ne.y - sw.y);
  
      //Fit padding in pixels
      var FIT_PAD = 40;
  
      for (var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom) {
        if (worldCoordWidth * (1 << zoom) + 2 * FIT_PAD < $(map.getDiv()).width() &&
          worldCoordHeight * (1 << zoom) + 2 * FIT_PAD < $(map.getDiv()).height())
          return zoom;
      }
      return 0;
    }
  }(jQuery))
  