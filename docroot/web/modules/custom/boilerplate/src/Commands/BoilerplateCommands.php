<?php

namespace Drupal\boilerplate\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Config\Entity\Query\Query;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;


/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class BoilerplateCommands extends DrushCommands {
  /**
   * Echos back update with the argument provided.
   *
   *
   *   Argument provided to the drush command.
   *
   * @command boilerplate:update
   * @aliases d9-update
   * @options arr An option that takes multiple values.
   * @options msg Whether or not an extra message should be displayed to the user.
   * @usage boilerplate:update akanksha --msg
   *   Display 'update Akanksha!' and a message.
   */
  public function update($options = ['msg' => FALSE]) {

    // Example Drush getting an image path and downloading from source

    // $query = \Drupal::entityQuery('node')
    //   ->condition('type', 'article');
    // $nids = $query->execute();
    // $nids = array_values($nids);

    // for ($i = $start; $i < count($nids); $i++) {

    //   $nid = $nids[$i];
    //   $node = Node::load($nid);
    //   if (isset($node->field_image_text->value)) {
    //     $uri = $node->field_image_text->value;
    //     $image_url = 'http://www.pathtoimage.com/content/data/multimedia/images/'.$uri;
    //     $new_file = file_get_contents($image_url);

    //     $file_name  = str_replace("/","_",$uri);

    //     $file = file_save_data($new_file, 'public://'.$file_name, FILE_EXISTS_REPLACE);
    //     // drupal_set_message('Node: ' . $file, 'status', FALSE);
    //     // Create node object with attached file.

    //     $node->field_image[] = [
    //       'target_id' => $file->id(),
    //       'alt' => 'Alt text'
    //     ];

    //     $node->save();
    //     drupal_set_message('Node: ' . $image_url, 'status', FALSE);
    //   }
    //   $code = $node->id();

    //   drupal_set_message('Node ' . $code . ' ' . $status_message, 'status', FALSE);
    // }

    // $class = 'Drupal\boilerplate\Commands\BoilerplateCommands';

    $this->output()->writeln('This is your first Drush 9 command.');
  }
}
