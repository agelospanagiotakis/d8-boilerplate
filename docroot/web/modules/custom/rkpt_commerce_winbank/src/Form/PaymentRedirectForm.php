<?php

namespace Drupal\rkpt_commerce_winbank\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PaymentRedirectForm.
 */
class PaymentRedirectForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'payment_redirect_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL)
    {
      $form_data = array(
        'AcquirerId' => \Drupal::request()->request->get('AcquirerId'),
        'MerchantId' => \Drupal::request()->request->get('MerchantId'),
        'PosId' => \Drupal::request()->request->get('PosId'),
        'User'  => \Drupal::request()->request->get('User'),
        'LanguageCode'  => \Drupal::request()->request->get('LanguageCode'),
        'MerchantReference'  => \Drupal::request()->request->get('MerchantReference'),
        'ParamBackLink'  => \Drupal::request()->request->get('ParamBackLink'),
        'bank' => \Drupal::request()->request->get('bank'),
      );


      $form['#attached']['library'][] = 'commerce_payment/offsite_redirect';
      foreach ($form_data as $key => $value) {
        $form[$key] = [
          '#type' => 'hidden',
          '#value' => $value,
          // Ensure the correct keys by sending values from the form root.
          '#parents' => [$key],
        ];
      }

      // The key is prefixed with 'commerce_' to prevent conflicts with $data.
      $form['commerce_message'] = [
        '#markup' => '<div class="checkout-help">' . t('Redirecting to bank. Please wait') . '</div>',
        '#weight' => -10,
        // Plugin forms are embedded using #process, so it's too late to attach
        // another #process to $form itself, it must be on a sub-element.
        '#process' => [
          [get_class($this), 'processRedirectForm'],
        ],
        '#action' => \Drupal::request()->request->get('bank_url'),
      ];

      return $form;


    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {


    }

    public static function processRedirectForm(array $element, FormStateInterface $form_state, array &$complete_form) {
      $complete_form['#action'] = $element['#action'];
      $complete_form['#attributes']['class'][] = 'payment-redirect-form';
      unset($element['#action']);
      // The form actions are hidden by default, but needed in this case.
      $complete_form['actions']['#access'] = TRUE;
      foreach (Element::children($complete_form['actions']) as $element_name) {
        $complete_form['actions'][$element_name]['#access'] = TRUE;
      }

      return $element;
    }

}
