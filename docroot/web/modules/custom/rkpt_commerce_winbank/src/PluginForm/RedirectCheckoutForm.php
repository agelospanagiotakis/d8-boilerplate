<?php

namespace Drupal\rkpt_commerce_winbank\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \SoapClient;

class RedirectCheckoutForm extends PaymentOffsiteForm {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $configuration = $this->getConfiguration();

    $language_code = $this->rkpt_commerce_winbank_get_current_lang_for_winbank();
    $default_currency = $payment->getAmount()->getCurrencyCode();

    $available_currencies = $this->rkpt_commerce_winbank_available_currencies();

    $CurrencyCode = $available_currencies[$default_currency];
    $amount = $payment->getAmount()->getNumber();
    $orderId = $payment->getOrderId();
    // kint($payment->getOrder());
    // die;

    $data['AcquirerId'] = $configuration['acquirer_id'];
    $data['MerchantId'] = $configuration['merchand_id'];
    $data['PosId'] = $configuration['pos_id'];
    $data['Username'] = $configuration['piraeus_username'];
    $data['Password'] = $configuration['piraeus_password'];

    $data['MerchantReference'] = $orderId.'_'.time(); // $order->order_id
    $data['RequestType'] = '02';
    $data['ExpirePreauth'] = '0';
    $data['Amount'] = $amount;
    $data['CurrencyCode'] = $CurrencyCode;
    $data['Installments'] = '0';
    $data['Bnpl'] = '0';
    $data['Parameters'] = 'order_id='.$orderId; // $order->order_id

    $ticket = $this->get_ticket($data);

    $form_data = array(
      'AcquirerId' => $data['AcquirerId'],
      'MerchantId' => $data['MerchantId'],
      'PosId' => $data['PosId'],
      'User'  => $data['Username'],
      'LanguageCode'  => $language_code,
      'MerchantReference'  => $data['MerchantReference'],
      'ParamBackLink'  => $data['Parameters'],
      'bank' => 'winbank',
      'bank_url' => 'https://paycenter.piraeusbank.gr/redirection/pay.aspx'
    );

    return $this->buildRedirectForm(
      $form,
      $form_state,
      '/checkout/static/payment',
      $form_data,
      PaymentOffsiteForm::REDIRECT_POST
    );
  }

  public function onReturn(OrderInterface $order, Request $request) {
      if ($request->something_that_marks_a_failure) {
          throw new PaymentGatewayException('Payment failed!');
      }

      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'completed',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $order->id(),
        'remote_id' => $request->request->get('remote_id'),
        'remote_state' => $request->request->get('remote_state'),
      ]);

      $payment->save();
  }

  private function getConfiguration() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\rkpt_commerce_winbank\Plugin\Commerce\PaymentGateway\RedirectCheckout $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    return $payment_gateway_plugin->getConfiguration();
  }

  /**
   * Get ticket form winbank with SoapClient.
   *
   * @see https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx?WSDL
   *
   * @inheritdoc
   */
  private function get_ticket(array $data) {

    $amount = $data['Amount'];
    $currency_code = $data['CurrencyCode'];

    $password = md5($data['Password']);

    $client = new SoapClient("https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx?WSDL");

    $request = array(
      'Username' => $data['Username'],
      'Password' => $password,
      'MerchantId' => $data['MerchantId'],
      'PosId' => $data['PosId'],
      'AcquirerId' => $data['AcquirerId'],
      'MerchantReference' => $data['MerchantReference'],
      'RequestType' => '02',
      'ExpirePreauth' => '0',
      'Amount' => $amount,
      'CurrencyCode' => $data['CurrencyCode'],
      'Installments' => '0',
      'Bnpl' => '0',
      'Parameters' => $data['Parameters']
    );

    $_SESSION['koobebe']['ticket_request']= $request;

    $result = $client->IssueNewTicket(array('Request' => $request));

    $result = array(
      'order_id' => $data['MerchantReference'],
      'result_code' => $result->IssueNewTicketResult->ResultCode,
      'result_description' => $result->IssueNewTicketResult->ResultDescription,
      'trans_ticket' => $result->IssueNewTicketResult->TranTicket,
      'timestamp' => $result->IssueNewTicketResult->Timestamp,
      'minutes_to_expiration' => $result->IssueNewTicketResult->MinutesToExpiration,
    );
    // kint($result);
    // die;
    $_SESSION['koobebe']['ticket_response'] = $result;

  }

  /**
   * Language for winbank interface.
   */
  private function rkpt_commerce_winbank_get_current_lang_for_winbank() {
    $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();

    if ($lang_code == 'el') {
      return 'el-GR';
    }

    return 'en-US';
  }

  /**
   * Supported currencies.
   */
  private function rkpt_commerce_winbank_available_currencies() {
    /*  'GRD' => '300', /* Just in case */
    return array('EUR' => '978', 'USD' => '840');
  }

}
