<?php

namespace Drupal\rkpt_commerce_winbank\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * This is a dummy controller for mocking an off-site gateway.
 */
class StaticUrlController implements ContainerInjectionInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new StaticUrlController object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Callback method which accepts POST.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function post() {
    // kint('post');
    // kint($this->currentRequest->request->get('form'));
    // kint(\Drupal::request()->request->get('AcquirerId'));
    // die;

    // $post_parameters = $this->currentRequest->request->parameters;


    $form_data = array(
      'AcquirerId' => \Drupal::request()->request->get('AcquirerId'),
      'MerchantId' => \Drupal::request()->request->get('MerchantId'),
      'PosId' => \Drupal::request()->request->get('PosId'),
      'User'  => \Drupal::request()->request->get('Username'),
      'LanguageCode'  => \Drupal::request()->request->get('LanguageCode'),
      'MerchantReference'  => \Drupal::request()->request->get('MerchantReference'),
      'ParamBackLink'  => \Drupal::request()->request->get('ParamBackLink'),
      'bank' => \Drupal::request()->request->get('bank'),
    );

    // return $this->buildRedirectForm(
    //   $form,
    //   $form_state,
    //   '/checkout/static/payment',
    //   $form_data,
    //   PaymentOffsiteForm::REDIRECT_POST
    // );


    // $response_form = \Drupal::httpClient()->post('https://paycenter.piraeusbank.gr/redirection/pay.aspx', [
    //       'verify' => true,
    //       'form_params' => $form_data,
    //         'headers' => [
    //           'Content-type' => 'application/x-www-form-urlencoded',
    //         ],
    //     ])->getBody()->getContents();
    // kint($response);
    // die;


    // $cancel = $this->currentRequest->request->get('cancel');
    // $return = $this->currentRequest->request->get('return');
    // $total = $this->currentRequest->request->get('total');

    // if ($total > 20) {
    //   return new TrustedRedirectResponse($return);
    // }
    $response = new Response();
    $response->setContent($response_form);
    // kint($response);
    return $response;
    // return array(
    //   '#markup' => $response,
    // );

    // return new TrustedRedirectResponse($response);
  }

  /**
   * Callback method which reacts to GET from a 302 redirect.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function on302() {

    $cancel = $this->currentRequest->query->get('cancel');
    $return = $this->currentRequest->query->get('return');
    $total = $this->currentRequest->query->get('total');

    if ($total > 20) {
      return new TrustedRedirectResponse($return);
    }

    return new TrustedRedirectResponse($cancel);
  }

}
