<?php

namespace Drupal\rkpt_commerce_winbank\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Render\BubbleableMetadata;

class PaymentInboundPathProcessor implements InboundPathProcessorInterface {

  function processInbound($path, Request $request) {
    $original_path = $path;
    $parts = array_filter(explode('/', $original_path));

    if (count($parts) == 3 && $parts['1'] == 'checkout'  && $parts['3'] == 'payment') {
      $path = 'checkout/' . $_SESSION['CSCU_ORDER_SESSION_TAG'] . '/payment';
      return $path;
    } else {
      return $path;
    }
  }
}
