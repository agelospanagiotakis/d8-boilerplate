<?php

namespace Drupal\rkpt_commerce_winbank\PathProcessor;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Render\BubbleableMetadata;

class PaymentOutboundPathProcessor implements OutboundPathProcessorInterface {

  public function processOutbound($path, &$options = array(), Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    $original_path = $path;
    $parts = array_filter(explode('/', $original_path));

    if (count($parts) == 3 && $parts['1'] == 'checkout'  && $parts['3'] == 'payment') {
      return $original_path;
    } else {
      return $original_path;
    }
  }
}
