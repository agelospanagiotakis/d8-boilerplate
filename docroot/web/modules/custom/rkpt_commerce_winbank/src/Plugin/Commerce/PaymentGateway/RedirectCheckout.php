<?php

namespace Drupal\rkpt_commerce_winbank\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Winbank offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "winbank_redirect_checkout",
 *   label = @Translation("Winbank (Redirect to winbank)"),
 *   display_label = @Translation("Winbank"),
 *    forms = {
 *     "offsite-payment" = "Drupal\rkpt_commerce_winbank\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\rkpt_commerce_winbank\PluginForm\RedirectCheckoutForm",
 *   },
 * )
 */
class RedirectCheckout extends OffsitePaymentGatewayBase {

  public function defaultConfiguration() {
    return [
        'acquirer_id' => '',
        'merchand_id' => '',
        'pos_id' => '',
        'piraeus_username' => '',
        'piraeus_password' => '',
        // 'private_key' => '',
        // 'api_key' => '',
      ] + parent::defaultConfiguration();
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['acquirer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Acquirer ID'),
      '#description' => $this->t('This is the Acquirer ID from the Winbank manager.'),
      '#default_value' => $this->configuration['acquirer_id'],
      '#required' => TRUE,
    ];

    $form['merchand_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchand ID'),
      '#description' => $this->t('This is the Merchand ID from the Winbank manager.'),
      '#default_value' => $this->configuration['merchand_id'],
      '#required' => TRUE,
    ];

    $form['pos_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('POS ID'),
      '#description' => $this->t('This is the POS ID from the Winbank manager.'),
      '#default_value' => $this->configuration['pos_id'],
      '#required' => TRUE,
    ];

    $form['piraeus_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Piraeus Username'),
      '#description' => $this->t('This is the Piraeus Username from the Winbank manager.'),
      '#default_value' => $this->configuration['piraeus_username'],
      '#required' => TRUE,
    ];

    $form['piraeus_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Piraeus Password'),
      '#description' => $this->t('This is the Piraeus Password from the Winbank manager.'),
      '#default_value' => $this->configuration['piraeus_password'],
      '#required' => TRUE,
    ];

    // $form['private_key'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Private key'),
    //   '#description' => $this->t('This is the private key from the Winbank manager.'),
    //   '#default_value' => $this->configuration['private_key'],
    //   '#required' => TRUE,
    // ];

    // $form['api_key'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('API key'),
    //   '#description' => $this->t('The API key for the same user as used in Agreement ID.'),
    //   '#default_value' => $this->configuration['api_key'],
    //   '#required' => TRUE,
    // ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['acquirer_id'] = $values['acquirer_id'];
    $this->configuration['merchand_id'] = $values['merchand_id'];
    $this->configuration['pos_id'] = $values['pos_id'];
    $this->configuration['piraeus_username'] = $values['piraeus_username'];
    $this->configuration['piraeus_password'] = $values['piraeus_password'];

    // $this->configuration['private_key'] = $values['private_key'];
    // $this->configuration['api_key'] = $values['api_key'];
  }

}
