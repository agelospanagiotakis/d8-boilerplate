## Setup

Edit .env file to change project name and docker image versions
Edit docker-compose or docker-compose-mac and docker-sync yaml files if you want to change deployed services.

Then open the project folder in your command line and run docker-compose:

```bash
docker-compose up -d
```
on linux OR
```bash
docker-sync-stack start
```
on macOS

and in order to enter php docker container (the one which contains drush, composer etc):
```bash
docker-compose exec php bash
```

## Notes

- files folder in a symlink from sites/default/files to /mnt/files
- settings.php file is generated from env variables on runtime. Just run init_drupal on php container on first run
